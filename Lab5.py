# 1. Haciendo uso de un ciclo anidado FOR simule que: en cada una de las entidades, cada
# persona realiza un pago de cada servicio. 7 puntos

'''
entidad=["BN Popular","BN Nacional","BN Central de Costa Rica"]
persona=["Juan","Alberto","Yesennia"]
servicio=["Servicio Municipalidades","Pago Luz","Pago Agua"]

for i in entidad:
    for j in persona:
        for c in servicio:
            print ("Deposito en: ",i,"de: ",j,"por concepto de: ",c)

'''


# 2. Haciendo uso de un ciclo WHILE, realice un menú que tenga 3 Opciones principales y
# cada una de estas opciones tenga dos opciones internas. No es necesario realizar ninguna
# acción, únicamente demostrar el comportamiento de los ciclos correctamente aplicados
# en un menú. 10 puntos

secundaria=0
opcion=0

def opcionPrincipal1():
    secundaria=int(input("Elija una nueva opcion: \n"
                    "1-Opcion Secuandaria 1\n"
                    "2-Opcion Principal 2\n"
                    "3-Regresar\n"
                    "Elija una Opcion: "))
    if secundaria==1:
        print("Opcion Secundaria 1")
        print(secundaria)
    elif secundaria==2:
        print("Opcion Secundaria 2")
        print(secundaria)
    elif secundaria==3:
        return menu()
    else:
        print("Opción incorrecta, elija de nuevo")
        print(secundaria)


def opcionPrincipal2():
    secundaria=int(input("Elija una nueva opcion: \n"
                    "1-Opcion Secuandaria 1\n"
                    "2-Opcion Principal 2\n"
                    "3-Regresar\n"
                    "Elija una Opcion: "))
    if secundaria==1:
        print("Opcion Secundaria 1")
        print(secundaria)
    elif secundaria==2:
        print("Opcion Secundaria 2")
        print(secundaria)
    elif secundaria==3:
        return(menu())
    else:
        print("Opción incorrecta, elija de nuevo")
        print(secundaria)
def opcionPrincipal3():
    secundaria=int(input("Elija una nueva opcion: \n"
                    "1-Opcion Secuandaria 1\n"
                    "2-Opcion Principal 2\n"
                    "3-Regresar\n"
                    "Elija una Opcion: "))
    if secundaria==1:
        print("Opcion Secundaria 1")
        print(secundaria)
    elif secundaria==2:
        print("Opcion Secundaria 2")
        print(secundaria)
    elif secundaria==3:
        return(menu())
    else:
        print("Opción incorrecta, elija de nuevo")
        print(secundaria)


def menu():
    opcion=int(input("Menu Principal: \n"
                     "1-Opcion Principal 1\n"
                     "2-Opcion Principal 2\n"
                     "3-Opcion Principal 3\n"
                     "4-Salir\n"
                     "Elija una Opcion: "))
    while opcion==4:
        print("Programa terminado")
        if opcion==1:
            return (opcionPrincipal1())
        elif opcion==2:
            return (opcionPrincipal2())
        elif opcion==3:
            return (opcionPrincipal3())
        else:
            print("Programa terminado")

menu()
opcionPrincipal1()
opcionPrincipal2()
opcionPrincipal3()



# 3. Desarrolle un ejercicio programado, donde ejemplifique el uso de la herencia, el ejemplo
# no puede ser los mismos vistos en clase, debe ser de autoría del estudiante. Este ejemplo
# debe tener las clases, la instancia y el consumo de los atributos y métodos de la clase de
# la cual hereda.

'''
class Muebles:
    def __init__(self,pMadera):
        self.madera=pMadera

    def getMaterial(self):
        return self.madera

class Muebleria(Muebles):
    def __init__(self,pMadera,pTrabajadores):
        super().__init__(pMadera)
        self.trabajadores=pTrabajadores

    def getTotal(self):
        return self.madera

objeto=Muebleria("Cedro","3")

print(objeto.getMaterial())
print(objeto.getTotal())
'''

# 4. El siguiente ejemplo de código contiene errores corríjalos para que el ejemplo se ejecute
# de manera correcta, indique en comentarios cuales fueron los errores encontrados. 25 puntos


'''

class Articulo:
    def __init__(self,pcodigo,pnombre):
        self.codigo=pcodigo
        self.nombre=pnombre

    def getNombre(self):
        return self.nombre

    def getColor(self):
        return self.codigo

class Juguete(Articulo):
    def __init__(self,pcodigo, pnombre, pprecio):
        super().__init__(pcodigo,pnombre)
        self.__precio=pprecio

    def getDescription(self):
        return self.getNombre()+self.__precio + "de color: " + self.getColor()

objeto=Juguete("01","Muñeco","3400")


print(objeto.getDescription())
print(objeto.getNombre())

'''
